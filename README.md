# Install All Google Fonts At Once

1. Create a folder on your desktop named "download".
2. Create a folder on your desktop named "fonts".
3. Go to https://github.com/google/fonts/tarball/master and save the zip file.
4. Open the zip file and extract all content to the desktop folder "download".
5. Open notepad
6. Paste the content shown above and replace NAME with the name of the logged user.
7. Go to File > Save as. Name the file "fonts.bat" and make sure u change the file type to "All files". Save the file on your desktop.
8. Right click fonts.bat and click "Execute as administrator".
9. All font files have been moved from the folder "download" to the folder "fonts".
10. Click start en type "fonts" > this will open up your font library.
11. Now just select all font files in the folder "fonts" with ctrl + a and drag-and-drop them onto you fonts library. This will install all 1700+ fonts.